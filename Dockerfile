FROM node:lts
WORKDIR /usr/src/app
COPY simple-app/package*.json ./
RUN npm install
COPY . .
EXPOSE 3000
CMD ["node", "app.js"]
