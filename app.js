// Import modul yang diperlukan
const express = require('express');
const app = express();
const port = 3000;

// Definisikan route untuk halaman utama
app.get('/', (req, res) => {
  res.send('Selamat datang di Aplikasi Node.js sederhana!');
});

// Mulai server pada port tertentu
app.listen(port, () => {
  console.log(`Aplikasi berjalan pada http://localhost:${port}`);
});
